var Promise = require("bluebird");

function doSomething(i) {
  return Promise.try(function() {
    return someAsyncOperation(i);
  }).then(function(result) {
    if (result === "foobar") {
      return doSomething(i + 1);
    } else {
      return result;
    }
  })
}

Promise.try(function() {
  return doSomething(0);
}).then(function(finalResult) {
  console.log("Done!", finalResult);
})